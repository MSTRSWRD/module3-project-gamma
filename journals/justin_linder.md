
Day 1, Mon 6/26 -
Today we started our project by cloning the provided repo. Our first steps were to establish authorization with the ability for users to log in and log out. Austin took the lead while Walker, Antonio, and I observed and reviewed reference material on how to complete the task. Tomorrow we will attempt to finalize and test authorization and stand up our DB using mongo.

Day 2, Tue 6/27 -
Came together this morning and discussed what we had worked on last night and wanted to start with today. Current status on Auth is non-functional despite both Austin and myself reviewing the provided video lectures with a fine tooth comb and applying what was there. I was able to get through about half of Curtis' lecture on auth and half of Riley's lecture on setting up Docker with mongo db. Was able to get containers to spin up successfully. Had to pivot to complete the video lectures on FastAPI, but these are all guaged to a postgres db, so not terribly helpful for us. Today, I will start with finishing reviewing those lectures, then Austin and I plan to pair up and try to resolve Auth and get it functional, which is our hope/goal to have running by EOD.

Day 3, Wed 6/28 -
Yesterday, we were able to get auth functional, but need to make a few adjustments to ensure that the duplicate account error is thrown when multiple of the same account are added. Was able to get some more details on the pipeline issue from git and a workaround to skip that file. I created an env file and generated a new SIGNING_KEY for deeper security and so that it was ignored by GIT. Today, plan is to add more routes for favorites, finish ingredients, and start meals.

Day 4, Thu 6/29 -
Yesterday we were nearly able to complete all of the routes. Walker and Antonio are going to take the lead on finalizing the ones we need for Meals. Today, the plan is to have those completed, tested, and functional. Then, we will begin to work on setting up our API on hopefully get those endpoints functional.

Day 5, Fri 6/30 -
Yesterday we worked through collectively getting the API to respond with recipe details while Walker did the coding. Things came together and we successfully tested and have a working API! Today, our plan is to refine that shape of data and ensure that we have what we need based on what comes back to inform our front end.

Day 6, Mon 7/10 -
Last week, we successfully wrapped up our backend and API endpoints. We were also able to get our spoonacularAPI returning with the results we were hoping for. Today our plan is to regroup, catchup, and review some of redux. Each of us want to spend some time going through the redux learn and becoming more familiar so that we can leverage it in our app.

Day 7, Tue 7/11 -
Yesterday, we were able to get caught up on the Redux learns and started taking a look at frontend auth. Today, the plan is to do some group coding where someone codes and the rest of us observe and comment. Walker is planning to start. We have agreed that this approach has been beneficial for our collective learning on these new topics. The goals for today are to hopefully have functional login, logout, and account creation pages.

Day 8, Wed 7/12 -
Yesterday we ran into a few issues with getting started on front end authorization and starting to get react going. We are feeling a little confused on converting things to redux or at least how to get it going from scratch, but feeling confident we can get things going. The plan today is to add some bootstrap so that things present well, get a few placeholder pages in, and then hopefully get the login, logout functionality working.

Day 9, Thu 7/13 -
Yesterday we nearly were able to get our login working on the front end. Austin was able to put in some extra time last night and cracked it though! Nice. We also were able to get the login form completed, our apislice completed, as well as our rtx store. Today the plan is to validate login/logout as well as get our signup form completed and functional. We'd also like to have our routes and navs done.

Day 10, Fri 7/14 -
Yesterday, we were able to get login, logout, and signup working. We were also able to get our ingredient form completed and an ingredient list that displays all listed ingredients finished. Today, we plan on completing our unit tests and trying to get the ingredient form and list component to display on the same page.

Day 11, Mon 7/17 -
Last week, we wrapped up by getting our tests written and working (we estimate that we are about 75-80% complete). Today, we would like to get a few components rounded out and have our pages able to be displayed and navigable.

Day 12, Tue 7/18 -
Yesterday, we broke into 2 groups to keep making progress as paired programmers. Austin and I were able to get our main page structure in place with some nice images, looping videos, and brief explanations on what PantryPal does. Looks great! Antonio was able to do some fine tuning later last night as well. Today, we would like to continue to finish our pages structure and having the links properly working to each respective page.

Day 13, Wed 7/19 -
Yesterday, we continued with the paired programming format. Austin and I focused on starting to work on the pantry page as well as clean up of routes and functionality. I was able to fix the routing on the signup sheet that logs the user in and lead them to the pantry page (also added a signup button to the about section on our main page). Login also leads to the pantry page and logout leads back to the main page. Today, we will continue to work on the pantry page and attempt to add in some errors where necessary.

Day 14, Thu 7/20 -
Yesterday, we continued on a paired programming where Austin kept working on the pantry page while I was able to refine some additional items on the main page including the footer and nav bar. I was also able to get some error handling in place and prompt errors on both the signup and login pages.
After doing some testing, I discovered a bug where the previous users data was still present on the page until a refresh was executed. We will be looking into figuring out how to clear out that cache so that when a new user logs in, they are immediately presented with their own data. A goal for today would be to have the user flow smoothed out i.e. user can login, add ingredients, generate a recipe, and potentially favorite and/or assign to a meal plan.

Day 15, Mon 7/24 -
Last Thursday, we were able to figure out the issue with the previous users details staying in place. I was also able to add favorite components and (re-add?) the appropriate slices. Looking to get those implemented and tested, hopefully by the end of the day. Over the weekend, Antonio did some AMAZING work getting a calendar structure in place and responsive to date time. This brings us to a great place and heading into our final week well-positioned.

Day 16, Tue 7/25 -
Yesterday, we were able to do some work on the modals and presenting recipe details/instructions on the respective pages. Last night, I was able to get all of the tests working and functional, but this brought into place concerns with our models and how we are passing in certain models into others. Will revisit with the group today. We are going to continue to work on getting the models and modals refined while validating that our meal plan page is dynamic while cycling through meals as each day goes by.

Day 17, Wed 7/26 -
Yesterday, we kept running into issues where could is straight up missing after pulling from main. Can't figure it out. Trying to establish guidelines on how the team will push/pull going forward using the "git in a group" learn. Last night, I was able to get a functional favorite button working and added a link to a favorites list page that populates when the button is clicked. Today, we are hoping to start getting each of these components in place and functional so that we can start testing and validating.

Day 18, Thu 7/27 -
Yesterday, ran into a few other issues and erros with getting things to come together, but nonetheless made some progress. Last night, I worked on some favorite components to get them to grab the meal id and details to present on the favorites page, but hit a wall. This morning, I brought it to the standup and we worked through it together and found success! Today we are hoping to put some final touches on things and combine each of the components onto the pages where we would like them.

Day 19, Fri 7/28 -
Final Day. Yesterday we ended on very high vibes as everything had basically come together and was functional. Today we are putting some final touches on the project and fully on track to submit something we are super proud of on time.
