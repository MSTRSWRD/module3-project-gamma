Day 1 (6/26/2023)
Today we took a swing at authorization in mongodb.
Ran into quite a few blockers due to lack of resources in comparison to postgres.
I need to go back and watch Friday's lecture on FastAPI.

Day 2 (6/27/2023)
Still working on Auth.
Reviewing FastAPI request videos today.

Day 3 (6/28/2023)
Finished Auth. Working on backend API routing and getting everyone up to date on git merges.
Considering what routes we will need from our Spoonacular API.
Touching up Excalidraw as we go to diagram expected requested recipe data.

Day 4 (6/29/2023)
Walker and I started the day with a few routing bug fixes involving our meal API. After that, as a group we discussed which attributes we'd need from our 3rd party Spoonacular API. Later in the day we finished the API endpoints for Spoonacular.

Day 5 (6/30/2023)
With our backend endpoints complete, we're all getting our branches up to date and handling merge conflicts. Later in the day we plan on touching up our wireframe and discussing potential style aesthetics for our front-end.

Day 6 (7/10/2023)
Discussed as a group if we wanted to use Redux on our project. We decided we will be using it and spent the rest of the day reading documentation and watching videos covering redux.

Day 7 (7/11/2023)
Built out a small main page and navbar in order to test if Redux was working properly.

Day 8 (7/12/2023)
Continued building out our main page. Applied some CSS with bootstrap. Discussed as a group the differences between using BrowserRouter and creating our own using CreateBrowserRouter.

Day 9 (7/13/2023)
Started the morning with bugfixing type errors we made yesterday. Today's goal is to finish our sign up form and get the routes completed for our dashboard as well as our meal plan. By the end of the week we would like to be ready to start working on the design aspects of our app.

Day 10 (7/14/2023)
All of us spent the day writing tests for our api.

Day 11 (7/17/2023)
Stand ups. Talked about what we worked on over the weekend. Made goals for the week. Going to work on react components most of the week.

Day 12 (7/18/2023)
Worked in pairs building out components most the day. Spent a couple hours applying CSS/flexbox to our main landing page. Converted MP4 video to WEBM. Ran into multiple errors trying to create a modal.

Day 13 (7/19/2023)
Spent the morning bugfixing with Walker and using CSS to help Justin style a footer's layout. Starting the meal-plan calendar today. learning about CSS grid.

Day 14 (7/20/2023)
Having some bugs involving our cached account data. Today we will be creating additional backend endpoints that the meal-plan grid will rely on for displaying meals at the specified day and their choice of breakfast, lunch or dinner.

Day 15 (7/24/2023)
Made a plan for the upcoming week. Worked more on the Calendar. Almost done.

Day 16 (7/24/2023)
Date logic for Calendar is finished. Working through getting the finished data to display properly in it's associated container.

Day 17 (7/24/2023)
Finishing touches on Calendar. Small touch ups on CSS.

Day 18 (7/24/2023)
Refactoring some CSS. Discussed what needs to be done for the final project.

Day 19 (7/24/2023)
Finished the project! Putting final touches on before submission.
