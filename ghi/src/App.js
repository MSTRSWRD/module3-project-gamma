import { Outlet } from "react-router-dom";
import Nav from "./Components/Nav.jsx";
import Footer from "./Components/Footer.jsx";

function App() {
  return (
    <div>
      <style>
        @import url('https://fonts.googleapis.com/css2?family=Roboto&display=swap%27);
      </style>
      <Nav />
      <div>
        <Outlet />
      </div>
      <div className="App">
        <Footer />
      </div>
    </div>
  );
}
export default App;
