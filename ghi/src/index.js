import React from 'react';
import ReactDOM from 'react-dom/client';
import { Provider } from "react-redux";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import './index.css';
import App from './App';
import Main from "./Main";
import Pantry from "./Components/Pantry";
import LoginForm from './Components/LoginForm';
import SignUpForm from './Components/SignupForm';
import MealPlan from './Components/Meals/MealPlan';
import ErrorPage from './ErrorPage';
import { store } from "./app/store";
import reportWebVitals from './reportWebVitals';
import IngredientForm from './Components/Ingredients/IngredientForm'
import IngredientList from './Components/Ingredients/IngredientList';
import MealList from './Components/Meals/MealList';
import MealDetails from './Components/Meals/MealDetails';
import AddRecipeToPlan from './Components/Meals/AddRecipeToPlan';
import RecipeGenerator from './Components/Recipes/RecipeGenerator';
import ModalDetails from './Components/ModalDetails';
import FavoritesList from './Components/Favorites/FavoritesList';
import PlaceholderGenerator from './Components/Recipes/PlaceholderGenerator'
import AddMealToPlan from './Components/Meals/AddMealToPlan';


const domain = /https:\/\/[^/]+/;
const basename = process.env.PUBLIC_URL.replace(domain, "");


const router = createBrowserRouter([
  {
    element: <App />,
    errorElement: <ErrorPage />,
    children: [
      {
        path: "/PantryPal",
        element: <Main />
      },
      {
        path: "/login",
        element: <LoginForm />
      },
      {
        path: "/pantry",
        element: <Pantry />
      },
      {
        path: "/mealplan",
        element: <MealPlan />
      },
      {
        path: "/signup",
        element: <SignUpForm />
      },
      {
        path: "/ingredient",
        element: <IngredientForm />
      },
      {
        path: "/ingredients",
        element: < IngredientList />
      },
      {
        path: "/meals",
        element: <MealList />
      },
      {
        path: "/meals/:meal_id",
        element: <MealDetails />
      },
      {
        path: "/meals/add/:recipe_id",
        element: <AddRecipeToPlan/>
      },
      {
        path: "/meals/favorites/add/:meal_id",
        element: <AddMealToPlan />
      },
      {
        path: "/recipe/generate",
        element: <RecipeGenerator />
      },
      {
        path: "/modaldetails",
        element: <ModalDetails />
      },
      {
        path:'/favorites',
        element:<FavoritesList />
      },
      {
        path: '/placeholder/generate',
        element: <PlaceholderGenerator />
      },
    ]
  }
],[basename])
const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <Provider store={store}>
      <RouterProvider router={router} />
    </Provider>
  </React.StrictMode>
);
// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
