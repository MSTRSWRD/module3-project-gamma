import countertop_pan from './assets/countertop_pan.webm'
import "./App.css"
import { Link } from "react-router-dom" 

const Main = () => {
    return (
        <div className='main' id='video'>
            <div className='overlay'>
                <video src={countertop_pan} type='video/webm' autoPlay loop muted />
                    <div className='content-wrapper d-flex justify-content-center'>
                        <div className="content text-center">
                            <div className='vid-headers'>
                                <h1 className="hero-title display-5 fw-bold">PantryPal</h1>
                                <span className="hero-text lead" >
                                    Welcome to your PantryPal! Your ultimate culinary companion and handy recipe generator
                                </span>
                            </div>
                        </div>
                    </div>
                <div className='container marketing mt-3'>
                    <div className='row pt-5 pb-5'>
                        <div className="col-lg-4">
                            <div className="text-center">
                                <img className="rounded-circle" src="https://images.pexels.com/photos/12673629/pexels-photo-12673629.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1" alt="people preparing a meal while looking at tablet" width="200" height="200"/>
                                <h2 className="mt-2">Generate Recipes</h2>
                                <p className='m-0'>
                                    Discover new recipe ideas with our recipe generator using ingredients available in your pantry or fridge.
                                </p>
                            </div>
                        </div>
                        <div className="col-lg-4">
                            <div className="text-center">
                                <img className="rounded-circle" src="https://images.pexels.com/photos/1640775/pexels-photo-1640775.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1" alt="meals prepped from a plan" width="200" height="200"/>
                                <h2 className='mt-2'>Plan Meals</h2>
                                <p className='m-0'>
                                    Save your generated recipes to a dynamic weekly meal plan calendar.
                                </p>
                            </div>
                        </div>
                        <div className="col-lg-4">
                            <div className="text-center">
                                <img className="rounded-circle" src="https://images.pexels.com/photos/8951408/pexels-photo-8951408.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1" alt="woman about to enjoy a bite of sushi" width="200" height="200"/>
                                <h2 className="mt-2">Enjoy!</h2>
                                <p className='m-0'>
                                    Make delicious recipes, save time, and waste less food.
                                </p>
                            </div>
                        </div>
                    </div>
                    <hr className="featurette-divide mb-5"></hr>
                    <div className="row featurette d-flex align-items-center">
                        <div className="col-md-5 ">
                            <img className="featurette-image img-fluid mx-auto" src="https://images.pexels.com/photos/4440173/pexels-photo-4440173.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1" alt="pantry full of labeled jars full of ingredients" width="500px" height="500px"/>
                        </div>
                        <div className="col-md-7">
                            <div className="featurette-heading">
                                <h3 className='text-center'>
                                Ever wanted an assistant that helps you come up with your next meal?
                                </h3>
                            </div>
                            <div className='text-center mt-3'>
                                <span className="text-muted">PantryPal is your go to for planning out this week's meal.
                                Add ingredients and use our recipe generator to find new delicious recipes
                                that you can make at home! Favorite a recipe to view later, or add it to your Meal Plan so that you
                                never have to worry about what to eat ever again.</span>
                                <p className="lead mt-4">
                                    JOIN US.
                                </p>
                                <p>
                                    <Link className="btn btn-secondary" to="/signup" role="button">Sign me up</Link>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <hr className="featurette-divider"></hr>
            </div>
        </div>
    )
}
export default Main;
