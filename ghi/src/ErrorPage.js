import sadEgg from "./sadEgg.jpeg";
import Nav from "./Components/Nav";

const ErrorPage = () => {
  return (
    <div className="container">
      <Nav />
      <div className="mt-5">
        <h1>Page not found</h1>
        <img src={sadEgg} alt="sad egg"/>
      </div>
    </div>
  );
};

export default ErrorPage;
