import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

export const pantryApi = createApi({
  reducerPath: "pantryApi",
  baseQuery: fetchBaseQuery({
    baseUrl: process.env.REACT_APP_API_HOST,
  }),
  endpoints: (builder) => ({
    getAllIngredients: builder.query({
      query: () => ({
        url: "/api/ingredients", //typically make sure to not have the / at the beg..
        credentials: "include",
      }),
      transformResponse: (response) => response.ingredients,
      providesTags: ["Ingredients"],
    }),
    updateIngredients: builder.mutation({
      query: (ingredient_id) => ({
        url: `/api/ingredients/${ingredient_id}`,
        method: "PUT",
        credentials: "include",
      }),
      invalidatesTags: ["Ingredients"],
    }),
    deleteIngredient: builder.mutation({
      query: (ingredient_id) => ({
        url: `/api/ingredients/${ingredient_id}`,
        method: "DELETE",
        credentials: "include",
      }),
      invalidatesTags: ["Ingredients"],
    }),
    addIngredient: builder.mutation({
      query: (ingredient) => ({
        url: "/api/ingredients",
        method: "POST",
        body: { name: ingredient },
        credentials: "include",
      }),
      invalidatesTags: ["Ingredients"],
    }),
    getAllMeals: builder.query({
      query: () => ({
        url: "/api/meals",
        credentials: "include",
      }),
      transformResponse: (response) => response.meals,
      providesTags: ["Meals", "Account"],
    }),
    createMeal: builder.mutation({
      query: (meal) => ({
        url: "/api/meals/",
        method: "POST",
        body: meal,
        credentials: "include",
      }),
      invalidatesTags: ["Meals"],
    }),
    getMealDetails: builder.query({
      query: (meal_id) => ({
        url: `/api/meals/${meal_id}`,
        credentials: "include",
      }),
    }),
    getAllRecipes: builder.query({
      query: (params) => ({
        url: "api/recipes",
        credentials: "include",
        params: params,
      }),
      transformResponse: (response) => response.recipes,
    }),
    getRecipeDetails: builder.query({
      query: (recipe_id) => ({
        url: `api/recipes/details?id=${recipe_id}`,
      }),
    }),
    getAccount: builder.query({
      query: () => ({
        url: "/token",
        credentials: "include",
      }),
      transformResponse: (response) => (response ? response.account : null),
      providesTags: ["Account"],
    }),
    logout: builder.mutation({
      query: () => ({
        url: "/token",
        method: "DELETE",
        credentials: "include",
      }),
      invalidatesTags: ["Account", "Meals", "Ingredients", "Favorites"],
    }),
    login: builder.mutation({
      query: ({ username, password }) => {
        const body = new FormData();
        body.append("username", username);
        body.append("password", password);
        return {
          url: `/token`,
          method: `POST`,
          body,
          credentials: "include",
        };
      },
      invalidatesTags: ["Account", "Meals", "Ingredients", "Favorites"],
    }),
    signup: builder.mutation({
      query: (body) => ({
        url: `/api/accounts`,
        method: "POST",
        body,
        credentials: "include",
      }),
      invalidatesTags: ["Account"],
    }),
    getFavorites: builder.query({
      query: () => ({
        url: "/api/favorites",
        credentials: "include",
      }),
      transformResponse: (response) => response.favorites,
      providesTags: ["Favorites"],
    }),
    createFavorite: builder.mutation({
      query: (body) => ({
        url: "/api/favorites",
        body,
        method: "POST",
        credentials: "include",
      }),
      invalidatesTags: ["Favorites"],
    }),
    deleteFavorite: builder.mutation({
      query: (favorite_id) => ({
        url: `/api/favorites/${favorite_id}`,
        method: "DELETE",
        credentials: "include",
      }),
      invalidatesTags: ["Favorites"],
    }),
  }),
});

export const {
    useGetAccountQuery,
    useGetAllIngredientsQuery,
    useGetAllMealsQuery,
    useCreateMealMutation,
    useGetMealDetailsQuery,
    useGetAllRecipesQuery,
    useGetRecipeDetailsQuery,
    useLogoutMutation,
    useLoginMutation,
    useSignupMutation,
    useDeleteIngredientMutation,
    useUpdateIngredientsMutation,
    useAddIngredientMutation,
    useGetFavoritesQuery,
    useCreateFavoriteMutation,
    useDeleteFavoriteMutation
} = pantryApi;
