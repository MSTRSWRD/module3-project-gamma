import { configureStore } from "@reduxjs/toolkit";
import { setupListeners } from "@reduxjs/toolkit/query";
import { pantryApi } from "./apiSlice";


export const store = configureStore({
    reducer: {
        [pantryApi.reducerPath]: pantryApi.reducer,
    },
    middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(pantryApi.middleware)
});

setupListeners(store.dispatch);
