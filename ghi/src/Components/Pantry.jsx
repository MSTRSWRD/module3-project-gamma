import IngredientForm from "./Ingredients/IngredientForm.jsx";
import IngredientList from "./Ingredients/IngredientList.jsx";
import { useGetAllMealsQuery } from "../app/apiSlice";
import ModalDetails from "./ModalDetails";
import TodaysMealCard from "./Meals/TodaysMealsCard";

const Pantry = () => {
  const { data, isLoading } = useGetAllMealsQuery();

  if (isLoading) return <div>Packing your pantry...</div>;

  const currentTime = new Date().getTime();
  const breakfastTime = new Date().setHours(7, 0, 0, 0); // 7:00 AM
  const lunchTime = new Date().setHours(12, 0, 0, 0); // 12:00 PM
  const dinnerTime = new Date().setHours(18, 0, 0, 0); // 6:00 PM

  const currentDate = new Date();
  const year = currentDate.getFullYear();
  const month = String(currentDate.getMonth() + 1).padStart(2, "0");
  const day = String(currentDate.getDate()).padStart(2, "0");
  const todaysDate = `${year}-${month}-${day}`;

  let dayToDisplay = [];

  if (currentTime < breakfastTime) {
    // Before Breakfast
    dayToDisplay = [];
  } else if (currentTime < lunchTime) {
    // Between Breakfast and Lunch
    dayToDisplay = data.filter(
      (meal) =>
        meal.day_meal_time[0] === 0 && meal.day_meal_time[1] === todaysDate
    );
  } else if (currentTime < dinnerTime) {
    // Between Lunch and Dinner
    dayToDisplay = data.filter(
      (meal) =>
        meal.day_meal_time[0] === 1 && meal.day_meal_time[1] === todaysDate
    );
  } else {
    // After Dinner
    dayToDisplay = data.filter(
      (meal) =>
        meal.day_meal_time[0] === 2 && meal.day_meal_time[1] === todaysDate
    );
  }

  return (
    <>
      <div className="container">
        <h1 className="top-padding">Welcome to your Pantry!</h1>
        <hr className="mb-4" />
        <div className="container">
          <div className="row">
            {dayToDisplay.length > 0 ? (
              <div className="col-md-8">
                <div className="card shadow p-3 border-0 mb-5">
                  {/* Display the first meal in dayToDisplay */}
                  <h2>{dayToDisplay[0].title}</h2>
                  <img
                    className=""
                    src={dayToDisplay[0].image}
                    alt="Your next meal!"
                  />
                  <ModalDetails id={dayToDisplay[0].id} />
                </div>
              </div>
            ) : (
              <div className="col-md-8">
                <div className="card shadow p-3 border-0 mb-5">
                  <h2>No Recipe data available</h2>
                </div>
              </div>
            )}
            <div className="col-md-4">
              <div className="row overflow-auto">
                <div className="col-md-12">
                  <IngredientForm />
                </div>
                <div className="col-md-12 border overflow-scroll">
                  <IngredientList />
                </div>
              </div>
            </div>
          </div>
        </div>
        <hr />
        <TodaysMealCard />
      </div>
    </>
  );
};

export default Pantry;
