import { useGetMealDetailsQuery } from "../app/apiSlice";
import FavoriteButtons from "./Favorites/FavoriteButtons";


const ModalDetails = ({ id }) => {
  const { data: meal, isLoading } = useGetMealDetailsQuery(id);

  if (isLoading) return <div>Loading...</div>;



  if (meal) return (
    //  Modal Details
    <>
      {/* <!-- Button trigger modal --> */}
      <button
        type="button"
        className="btn btn-outline-dark"
        data-bs-toggle="modal"
        data-bs-target="#Backdrop"
      >
        View Recipe Details
      </button>

      {/* <!-- Modal --> */}
      <div
        className="modal modal-xl fade"
        id="Backdrop"
        data-bs-keyboard="false"
        tabIndex="-1"
        aria-labelledby="BackdropLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog modal-dialog-centered modal-dialog-scrollable">
          <div className="modal-content">
            <div className="modal-header">
              <h1 className="modal-title fs-5" id="BackdropLabel">
                {meal.title}
              </h1>
              <button
                type="button"
                className="btn-close"
                data-bs-dismiss="modal"
                aria-label="Close"
              ></button>
            </div>
            <div className="modal-body">
              <div className="container-fluid">
                <div className="row">
                  <div className="col-lg-4">
                    <img
                      src={meal.image}
                      alt=""
                      style={{ maxWidth: "100%", maxHeight: "100%" }}
                    />
                  </div>
                  <div className="col-lg-6 border overflow-auto">
                    <div className="overflow-auto">
                      <ul>
                        <li>Ready in Minutes - {meal.readyInMinutes} min.</li>
                      </ul>
                      <ul>
                        <li>Servings - {meal.servings}</li>
                      </ul>
                      <ul>
                        <li>Instructions</li>
                        <ol>
                          {meal.analyzedInstructions[0].steps.map(
                            (stp, index) => (
                              <li key={index}>
                                {stp.step}
                                <ul>
                                  <li>Ingredients</li>
                                  <ul>
                                    {stp.ingredients.map(
                                      (ingredient, ingredientIndex) => (
                                        <li key={ingredientIndex}>
                                          {ingredient.name}
                                        </li>
                                      )
                                    )}
                                  </ul>
                                </ul>
                              </li>
                            )
                          )}
                        </ol>
                      </ul>
                      <ul>
                        <li>Summary</li>
                        <ul>
                          <li>
                              <div dangerouslySetInnerHTML={{__html: meal.summary}}/>
                          </li>
                        </ul>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="modal-footer">
              <div className="col-4 text-end">
                  <FavoriteButtons name={meal.title} id={id} />
                </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default ModalDetails;
