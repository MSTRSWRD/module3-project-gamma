import {
  useGetAllIngredientsQuery,
  useDeleteIngredientMutation,
} from "../../app/apiSlice";

const IngredientList = () => {
  const { data, isLoading } = useGetAllIngredientsQuery();
  const [deleteIng] = useDeleteIngredientMutation();

  if (isLoading) return <div> Packing your pantry...</div>;

  return (
    <div className="container">
      <div className="row-4 ">
        <div className="table-container">
          <table className="table table-striped align-middle table-sm">
            <thead>
              <tr>
                <th className="sticky-header">Ingredients</th>
                <th className="sticky-header"></th>
              </tr>
            </thead>
            <tbody>
              {data.map((ing) => {
                return (
                  <tr key={ing.id}>
                    <td>{ing.name}</td>
                    <td>
                      <button
                        onClick={() => deleteIng(ing.id)}
                        type="button"
                        className="btn-sm btn-secondary float-end"
                      >
                        Delete
                      </button>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
};

export default IngredientList;
