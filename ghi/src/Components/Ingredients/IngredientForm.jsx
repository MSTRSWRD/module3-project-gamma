import { useState } from "react"
import { useAddIngredientMutation } from "../../app/apiSlice"



const IngredientForm = () => {
    const [ingredients] = useAddIngredientMutation()
    const [ingredient, setIngredient] = useState('')

    const handleChange = (setFunction) => {
        return (
            function(event) {
                setFunction(event.target.value)
            }
        )
    }

    const handleSubmit = (e) => {
        e.preventDefault()
        ingredients(ingredient)
        setIngredient('')
        document.getElementById("add-ingredient-form").reset()
    }

    return (
      <form id="add-ingredient-form" onSubmit={handleSubmit}>
        <div className="input-group mb-3">
          <input
            required
            type="text"
            className="form-control"
            placeholder="Add Ingredient..."
            aria-label="Add Ingredient..."
            aria-describedby="button-addon2"
            onChange={handleChange(setIngredient)}
          />
          <button
            className="btn btn-outline-secondary"
            type="submit"
            id="add-ingredient-form"
          >
            Submit
          </button>
        </div>
      </form>
    );
}

export default IngredientForm;
