import FavoritesCard from "./FavoritesCard";
import { useGetFavoritesQuery } from "../../app/apiSlice";

const FavoritesList = () => {
  const { data, isLoading } = useGetFavoritesQuery();

  if (isLoading) return <div>Retrieving Favorite Recipes...</div>;

  return (
    <div>
      <h2 className="text-center">Favorites</h2>
      <div className="card-container2 border border-3 border-dark">
        <div className="card-container1 p-1 mt-3 d-grid grid-cols-3 gap-4">
          {data.map((f) => (
            <FavoritesCard key={f.title} title={f.title} id={f.meal_id} />
          ))}
        </div>
      </div>
    </div>
  );
};

export default FavoritesList;
