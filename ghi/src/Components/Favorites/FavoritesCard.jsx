import { useGetMealDetailsQuery } from '../../app/apiSlice';
import FavoriteButtons from './FavoriteButtons';
import FavoriteDetailModal from './FavoriteDetailsModal';
import { useState } from 'react';
import AddFavoriteToPlanModal from './AddFavoriteToPlanModal';


const FavoritesCard = ({ title, id }) => {

    const [showDetailModal, setShowDetailModal] = useState(false)
    const [showAddModal, setShowAddModal] = useState(false)

    const { data, isLoading } = useGetMealDetailsQuery(id);
    if (isLoading) return <div>Loading Details...</div>

    return (
        <div className="card shadow" style={{ "width": "14rem", "marginRight": "15px", "marginButton": "15px"}}>
            <img src={data.image} className="card-img-top" alt={title} />
            <div className="card-body">
                <h5 className="card-title">{title}</h5>
            </div>
            <hr />
                <div className="card-footer-dark mb-1 p-2">
                    <button className="btn btn-primary" style={{ marginRight: '30px' }} onClick={() => setShowDetailModal(true)}>Details</button>
                        <button style={{"marginRight": "5px"}} type="button" className="btn btn-outline-secondary" onClick={() => setShowAddModal(true)}>
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-calendar-plus" viewBox="0 0 16 16">
                                <path d="M8 7a.5.5 0 0 1 .5.5V9H10a.5.5 0 0 1 0 1H8.5v1.5a.5.5 0 0 1-1 0V10H6a.5.5 0 0 1 0-1h1.5V7.5A.5.5 0 0 1 8 7z"></path>
                                <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z"></path>
                            </svg>
                            <span className="visually-hidden">Button</span>
                        </button>
                        <FavoriteButtons name={title} id={id} />
                </div>
                <FavoriteDetailModal meal_id={id} show={showDetailModal} onClose={() => setShowDetailModal(false)}/>
                <AddFavoriteToPlanModal meal_id={id} show={showAddModal} onClose={() => setShowAddModal(false)}/>
        </div>
    );
};

export default FavoritesCard;
