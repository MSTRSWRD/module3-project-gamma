import { useGetAllMealsQuery } from "../../app/apiSlice";

const CalendarContent = () => {
  const { data } = useGetAllMealsQuery();

  return (
    <div className="">
      {data.map((meal) => (
        <div key={meal.id}>
            <div className="border border-dark">
                <img src={meal.image} alt="" style={{ width: "200px", height: "150px" }} />
                <p>{meal.title}</p>
                <p>{meal.summary}</p>
            </div>
        </div>
      ))}
    </div>
  );
};

export default CalendarContent;
