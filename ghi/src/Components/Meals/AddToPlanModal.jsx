import AddRecipeToPlan from './AddRecipeToPlan'
// import { useEffect } from 'react'


const AddToPlanModal = ({recipe_id, onClose, show}) => {

    // const closeOnEscapePush = (e) => {
    //     if ((e.charCode || e.keyCode) === 27) {
    //         onClose()
    //     }
    // }

    // useEffect(() => {
    //     document.body.addEventListener('keydown', closeOnEscapePush)
    //     return function cleanup() {
    //         document.body.removeEventListener('keydown', closeOnEscapePush)
    //     }
    // }, [closeOnEscapePush]) 
    //added above

    if (!show) return null
    return (
                    <div className='clown-modal' onClick={onClose}>
                        <div className="circus-clown-modal-content" onClick={e => e.stopPropagation()}>
                            <div className="clown-modal-header">
                                <h3 className="clown-modal-title text-center">Add to Plan</h3>
                            </div>
                            <div className="clown-modal-body">
                                <AddRecipeToPlan recipe_id={recipe_id} onClose={onClose} />
                            </div>
                            <div className="clown-modal-footer" >
                                <button className="btn btn-primary" onClick={onClose}>Close</button>
                            </div>
                        </div>
                    </div>
            );
        }

export default AddToPlanModal
