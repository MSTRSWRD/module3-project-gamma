import { Link } from 'react-router-dom';
import FavoriteButtons from '../Favorites/FavoriteButtons';

const MealCard = ({name, img, id}) => {

    return (
        <div className="card mealplan-card">
            <div className="card-body">
                <h5 className="card-title">{name}</h5>
            <img src={img} className="card-img-top mb-2" alt={name} />
                <div className="card-footer-dark">
                    <button className="btn btn-success" style={{ marginRight: '72px' }}>
                        <Link className='mealplan-card-detail' to={`/meals/${id}`}>Details</Link>
                    </button>
                        <FavoriteButtons name={name} id={id} />
                </div>
            </div>
        </div>
    )
}

export default MealCard;
