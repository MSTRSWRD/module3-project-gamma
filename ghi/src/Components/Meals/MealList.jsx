import MealCard from "./MealCard";
import { useGetAllMealsQuery } from "../../app/apiSlice";

const MealList = () => {
    const {data, isLoading} = useGetAllMealsQuery()
    if (isLoading) return <div>Loading...</div>

    return (
        <div className="mt-3">
            <h1>
                Meals
            </h1>
            <div className="row mt-3">
                {data.map(m => <MealCard key={m.title} id={m.id} name={m.title} img={m.image}/>)}
            </div>
        </div>
    )


}

export default MealList;


