import { useGetMealDetailsQuery } from "../../app/apiSlice";
import { useParams, Link } from "react-router-dom";

const MealDetail = () => {
    const { meal_id } = useParams()
    const {data, isLoading} = useGetMealDetailsQuery(meal_id)

    if (isLoading) return <div>Loading...</div>
    return (
      <div>
        <div className="row">
          <div className="col-8">
            <h2>{data.title.toUpperCase()}</h2>
          </div>
          <div className="col-4 text-end">
            <button className="btn btn-success"><Link style={{"textDecoration": 'none', "color": "white"}} to={`/meals/favorites/add/${meal_id}`}>Add to Plan</Link></button>
          </div>
        </div>
        <ul className="list-group">
          <li className="list-group-item">Name: {data.title}</li>
          <li className="list-group-item">Summary: <div
                dangerouslySetInnerHTML={{__html: data.summary}}
            /></li>
          <li className="list-group-item">Prep Time: {data.readyInMinutes} Minutes</li>
        </ul>
      </div>
    );
}

export default MealDetail;
