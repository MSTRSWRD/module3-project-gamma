import meal_placeholder from "../../assets/meal_placeholder.jpg"

const ErrorCardBrk = () => {
  return (
    <>
      <div className="card">
        <div className="card-header">Breakfast</div>
        <img className="card-img-top" src={meal_placeholder} alt="" />
        <div className="card-body">
        </div>
      </div>
    </>
  );
};

export default ErrorCardBrk;
