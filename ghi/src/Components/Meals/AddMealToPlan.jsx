import { useState, useEffect } from "react";
import { useGetMealDetailsQuery, useCreateMealMutation } from "../../app/apiSlice";


const AddMealToPlan = ({meal_id, onClose}) => {

    const {data, isLoading} = useGetMealDetailsQuery(meal_id)
    const [createMeal] = useCreateMealMutation()

    const [formData, setFormData] = useState({
        day_meal_time: [],
        title: "",
        image: "",
        summary: "",
        servings: 0,
        readyInMinutes: 0,
        analyzedInstructions: []

    })

    useEffect(() => {
        if (data && data.title && data.image && data.summary) {
            setFormData((prevFormData) => ({
                ...prevFormData,
                title: data.title,
                image: data.image,
                summary: data.summary,
                servings: data.servings,
                readyInMinutes: data.readyInMinutes,
                analyzedInstructions: data.analyzedInstructions,
            }));
        }
    }, [data]);


    const handleChange = (event) => {
        setFormData({
            ...formData,
            [event.target.name]: event.target.value
        })
    }

  const handleSubmit = (e) => {
    e.preventDefault();

    // Calculate date based on selected day
    const daysOfWeek = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    const selectedDayIndex = daysOfWeek.indexOf(e.target.day.value);
    const currentDate = new Date();
    const currentDayOfWeekIndex = currentDate.getDay();

    // Calculate the difference in days between the current day and the selected day
    let daysToAdd = selectedDayIndex - currentDayOfWeekIndex;

    // If the selected day is before the current day, add a week's worth of days
    if (daysToAdd < 0) {
      daysToAdd += 7;
    }

    // Clone the current date and adjust it by the calculated daysToAdd
    const selectedDate = new Date(currentDate);
    selectedDate.setDate(currentDate.getDate() + daysToAdd);

    // Get the year, month, and day from the selected date
    const year = selectedDate.getFullYear();
    const month = String(selectedDate.getMonth() + 1).padStart(2, "0");
    const day = String(selectedDate.getDate()).padStart(2, "0");

    // Format the day as "year-month-day"
    const formattedDay = `${year}-${month}-${day}`;

     // Convert the meal to number (0 for breakfast, 1 for lunch, 2 for dinner)
    let mealNumericValue;
    switch (e.target.meal.value) {
      case "Breakfast":
        mealNumericValue = 0;
        break;
      case "Lunch":
        mealNumericValue = 1;
        break;
      case "Dinner":
        mealNumericValue = 2;
        break;
      default:
        mealNumericValue = -1;
    }

    // Reformats the formData before submission
    const formattedFormData = {
      ...formData,
      day_meal_time: [mealNumericValue, formattedDay]

    };

    createMeal(formattedFormData);

    setFormData({
      day_meal_time: [],
      title: "",
      image: "",
      summary: "",
      servings: 0,
      readyInMinutes: 0,
      analyzedInstructions: [],
    });
    document.getElementById("add-meal-form").reset()
    onClose()

  };

    if (isLoading) return <div>Loading...</div>
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h4>{data.title}</h4>
                    <form id="add-meal-form" onSubmit={handleSubmit}>
                        <div className="form-floating mb-3">
                            <select onChange={handleChange} required={true} placeholder="Day" type="text" name="day" id="day" className="form-select">
                                <option value="Sunday">Sunday</option>
                                <option value="Monday">Monday</option>
                                <option value="Tuesday">Tuesday</option>
                                <option value="Wednesday">Wednesday</option>
                                <option value="Thursday">Thursday</option>
                                <option value="Friday">Friday</option>
                                <option value="Saturday">Saturday</option>
                            </select>
                            <label htmlFor="day">Choose a Day...</label>
                        </div>

                        <div className="form-floating mb-3">
                            <select onChange={handleChange} required={true} placeholder="Meal" type="text" name="meal" id="meal" className="form-select">
                                <option value="Breakfast">Breakfast</option>
                                <option value="Lunch">Lunch</option>
                                <option value="Dinner">Dinner</option>
                            </select>
                            <label htmlFor="day">Choose a Meal...</label>
                        </div>
                        <div>

                        </div>
                        <button type="submit" className="btn btn-secondary">Add</button>
                    </form>
                </div>
            </div>
        </div>
    );

}

export default AddMealToPlan;
