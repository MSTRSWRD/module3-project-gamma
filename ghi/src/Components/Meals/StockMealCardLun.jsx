import meal_placeholder from "../../assets/meal_placeholder.jpg"


const ErrorCardLun = () => {
  return (
    <>
      <div className="card">
        <div className="card-header">Lunch</div>
        <img className="card-img-top" src={meal_placeholder} alt="" />
        <div className="card-body">
        </div>
      </div>
    </>
  );
};

export default ErrorCardLun;
