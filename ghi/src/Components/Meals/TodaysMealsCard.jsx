import { useGetAllMealsQuery } from "../../app/apiSlice";
import ErrorCardBrk from "./StockMealCardBrk";
import ErrorCardLun from "./StockMealCardLun";
import ErrorCardDin from "./StockMealCardDin";


const TodaysMealCard = () => {
    const { data, error, isLoading } = useGetAllMealsQuery();

    const currentDate = new Date();
    const year = currentDate.getFullYear();
    const month = String(currentDate.getMonth() + 1).padStart(2, '0');
    const day = String(currentDate.getDate()).padStart(2, '0');

    const todaysDate = `${year}-${month}-${day}`
    let todaysBreakfast = {}
    let todaysLunch = {}
    let todaysDinner = {}

    const breakfastMeals = data.filter(meal => meal.day_meal_time[0] === 0 && meal.day_meal_time[1] === todaysDate);
    const lunchMeals = data.filter(meal => meal.day_meal_time[0] === 1 && meal.day_meal_time[1] === todaysDate);
    const dinnerMeals = data.filter(meal => meal.day_meal_time[0] === 2 && meal.day_meal_time[1] === todaysDate);

    todaysBreakfast = breakfastMeals.map(meal => ({
        image: meal.image,
        title: meal.title,
        summary: meal.summary,
        mealTime: "Breakfast",
    }));

    todaysLunch = lunchMeals.map(meal => ({
        image: meal.image,
        title: meal.title,
        summary: meal.summary,
        mealTime: "Lunch",
    }));

    todaysDinner = dinnerMeals.map(meal => ({
        image: meal.image,
        title: meal.title,
        summary: meal.summary,
        mealTime: "Dinner",
    }));

    if (isLoading) {return <div>Loading...</div>}
    if (error) {return <div>waiting for you to add a meal!</div>}

  return (
    <div className="mb-5">
      <h4>Todays Meals</h4>
      <div className="container mt-3">
        <div className="row">
          <div className="col-md-4">
            {todaysBreakfast[0] ? (
              <div className="card">
                <div className="card-header">Breakfast</div>
                <img
                  className="card-img-top"
                  src={todaysBreakfast[0].image}
                  alt=""
                />
                <div className="card-body">
                  <h5 className="card-title">{todaysBreakfast[0].title}</h5>
                </div>
              </div>
            ) : (
              <ErrorCardBrk />
            )}
          </div>
          <div className="col-md-4">
            {todaysLunch[0] ? (
              <div className="card">
                <div className="card-header">Lunch</div>
                <img
                  className="card-img-top"
                  src={todaysLunch[0].image}
                  alt=""
                />
                <div className="card-body">
                  <h5 className="card-title">{todaysLunch[0].title}</h5>
                </div>
              </div>
            ) : (
              <ErrorCardLun />
            )}
          </div>
          <div className="col-md-4">
            {todaysDinner[0] ? (
              <div className="card">
                <div className="card-header">Dinner</div>
                <img
                  className="card-img-top"
                  src={todaysDinner[0].image}
                  alt=""
                />
                <div className="card-body">
                  <h5 className="card-title">{todaysDinner[0].title}</h5>
                </div>
              </div>
            ) : (
              <ErrorCardDin />
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default TodaysMealCard;
