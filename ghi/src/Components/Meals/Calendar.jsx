import { useGetAllMealsQuery } from "../../app/apiSlice";
import FavoriteButtons from "../Favorites/FavoriteButtons";

const Calendar = () => {
    const { data, isLoading } = useGetAllMealsQuery();

    let mealContainerId = [
        [0, 1, 2, 3, 4, 5, 6],
        [0, 1, 2, 3, 4, 5, 6],
        [0, 1, 2, 3, 4, 5, 6]
    ]

    mealContainerId = mealContainerId.map(slot => {
        return (
            slot.map(num => {
                let day = new Date(Date.now())
                day.setDate(day.getDate() + num)
                return new Date(day).toISOString().slice(0,10)
            })
        )
    })

    if (isLoading) {return <div>Loading...</div>;}

    const getDayName = (index) => {
        const daysOfWeek = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        const currentDate = new Date();
        const dayIndex = (currentDate.getDay() + (index)) % 7;
        return daysOfWeek[dayIndex];
    };

    return (
        <div className="mt-5">
            {/* Calendar header date/day */}
            <div className="row table-dates">
                {mealContainerId[0].map((date, index) => {
                    return (
                        <div className="date-columns col border border-dark" key={index}>
                            <div className="date-header">{date}</div>
                            <div>{getDayName(index)}</div>
                        </div>
                    );
                })}
            </div>
            {/* Calendar grid */}
            {mealContainerId.map((mealPlanSlot, timeIndex) => {
                return (
                    <div className="row" key={timeIndex}>
                        {mealPlanSlot.map((date, dateIndex) => {
                            return (
                                <div className="meal-columns col border border-dark d-flex align-items-center" key={dateIndex}>
                                    {data.filter(
                                        (mealData) =>
                                            mealData.day_meal_time[0] === timeIndex &&
                                            mealData.day_meal_time[1] === date
                                        )
                                        .map((meal) => {
                                            return (
                                                <div key={meal.id}>
                                                    <img src={meal.image} alt={meal.title} style={{height: "100px", width: "150px" }} />
                                                    <div style={{fontSize: "10pt"}}>{meal.title} <FavoriteButtons name={meal.title} id={meal.id} /></div>
                                                </div>
                                            );
                                        })}
                                </div>
                            );
                        })}
                    </div>
                );
            })}
        </div>
    );
};



export default Calendar;
