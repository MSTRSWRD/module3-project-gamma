import Calendar from "./Calendar";
import FavoritesList from "../Favorites/FavoritesList";
import PlaceholderGenerator from "../Recipes/PlaceholderGenerator";

const MealPlan = () => {
  return (
    <div className="mealplan-container text-center">
      <h1 className="top-padding">Manage your Meal Plan!</h1>
      <Calendar />
      <div className="text-start d-flex mt-5">
        <div className="col-4">
          <PlaceholderGenerator />
        </div>
        <div className="col-1"></div>
        <div className="col-7">
          <FavoritesList />
        </div>
      </div>
    </div>
  );
};

export default MealPlan;
