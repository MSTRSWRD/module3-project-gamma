import { useSignupMutation } from "../app/apiSlice";
import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import AlertError from "../AlertError";

const SignUpForm = () => {
  const navigate = useNavigate();
  const [signup, signupResult] = useSignupMutation();
  const [errorMessage, setErrorMessage] = useState("");
  const [full_name, setFullName] = useState("");
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  useEffect(() => {
    if(signupResult.error) {
      if(signupResult.error.status === 400) {
        setErrorMessage(signupResult.error.data.detail);
      }
    }
    if (signupResult.isSuccess) navigate('/pantry');
  }, [navigate,signupResult]);


  const handleSubmit = (e) => {
    e.preventDefault();
    signup({ full_name, username, password });
  };

  return (
    <div className="row nav-spacing">
      <div className="col-md-6 offset-md-3">
        <h1>Sign Up</h1>
        <form onSubmit={handleSubmit}>
          {errorMessage && <AlertError>{errorMessage}</AlertError>}
          <div className="mb-3">
            <label htmlFor="SignUp__fullname" className="form-label">
              Full Name
            </label>
            <input
              type="text"
              className="form-control"
              id="SignUp__fullname"
              value={full_name}
              onChange={(e) => {
                setFullName(e.target.value)
                setErrorMessage('')
              }}
            />
          </div>
          <div className="mb-3">
            <label htmlFor="SignUp__username" className="form-label">
              Username
            </label>
            <input
              type="text"
              className="form-control"
              id="SignUp__username"
              value={username}
              onChange={(e) => {
                setUsername(e.target.value)
                setErrorMessage("")
            }}
            />
          </div>
          <div className="mb-3">
            <label htmlFor="SignUp__password" className="form-label">
              Password
            </label>
            <input
              type="password"
              className="form-control"
              id="SignUp__password"
              value={password}
              onChange={(e) => {
                setPassword(e.target.value)
                setErrorMessage('')
            }}
            />
          </div>
          <button type="submit" className="btn btn-success">
            Submit
          </button>
        </form>
      </div>
    </div>
  );
};

export default SignUpForm;
