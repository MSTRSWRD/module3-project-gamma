import { useState } from "react";
import generate_recipe from "../../assets/generate_recipe.jpg";
import RecipeGenerator from "./RecipeGenerator";

const PlaceholderGenerator = () => {
  const [clicked, setClicked] = useState(false);

  const handleClick = () => {
    setClicked(true);
  };

  return (
    <>
      {clicked ? (
        <RecipeGenerator />
      ) : (
        <div className="text-center">
          <button onClick={handleClick}>
            <img src={generate_recipe} width={250} height={250} alt="placeholder" />
          </button>
        </div>
      )}
    </>
  );
};

export default PlaceholderGenerator;
