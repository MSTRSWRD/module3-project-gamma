import { useState } from 'react';
import RecipeModal from './RecipeModal';
import AddToPlanModal from '../Meals/AddToPlanModal';


const RecipeCard = ({recipe}) => {

    const [showDetailModal, setShowDetailModal] = useState(false);
    const [showPlanModal, setShowPlanModal] = useState(false)

    const handleClosePlanModal = () => {
        setShowPlanModal(false)
    }

    return (
        <div className="card shadow" style={{"width": "12rem", "marginRight": "15px", "marginBottom": "15px"}}>
            <h5 className="card-title" style={{'marginLeft': '3px'}}>{recipe.title}</h5>
            <div>
            </div>
            <div className="card-body">
                <img src={recipe.image} className="card-img-top" alt={recipe.title} />
                <button className="btn btn-primary" style={{"marginRight": "0px", "marginTop": "15px"}} onClick={() => setShowDetailModal(true)}>Details</button>

                <button style={{"marginLeft": "30px", "marginTop": "15px"}} type="button" className="btn btn-outline-secondary" onClick={() => setShowPlanModal(true)}>
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-calendar-plus" viewBox="0 0 16 16">
                        <path d="M8 7a.5.5 0 0 1 .5.5V9H10a.5.5 0 0 1 0 1H8.5v1.5a.5.5 0 0 1-1 0V10H6a.5.5 0 0 1 0-1h1.5V7.5A.5.5 0 0 1 8 7z"></path>
                        <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z"></path>
                    </svg>
                    <span className="visually-hidden">Button</span>
                </button>
            </div>
                <AddToPlanModal recipe_id={recipe.id} show={showPlanModal} onClose={handleClosePlanModal}/>
                <RecipeModal recipe_id={recipe.id} show={showDetailModal} onClose={() => setShowDetailModal(false)} />
        </div>
    )
}

export default RecipeCard;
