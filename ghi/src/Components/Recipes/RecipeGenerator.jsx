import { useGetAllRecipesQuery, useGetAllIngredientsQuery } from "../../app/apiSlice";
import RecipeCard from "./RecipeCard";
import { useState } from "react";


const RecipeGenerator = () => {
    const {data:ingredient, loading} = useGetAllIngredientsQuery()
    const [num, setNum] = useState(1)
    const [clicked, setClicked] = useState(false)

    const ingredientArray = []
    for (const i in ingredient) {
        ingredientArray.push(ingredient[i].name)
    }

    let params = {
        "ingredient": ingredientArray,
        "number": num
    }
    const {data: recipes, isLoading} = useGetAllRecipesQuery(params)
    const wait = (ms) => {
        return new Promise((resolve) => setTimeout(resolve, ms))
    }
    async function handleClick() {
        const numb = document.getElementById('recipeNum').value
        if (numb) {
            setNum(numb)
            await wait(400)
            setClicked(true)
        }
    }


    if (isLoading || loading) return <div>Loading...</div>
    return (
      <div>
        <h2 className="text-center">Generate Recipes</h2>
        <div className="row p-3 border border-3 border-dark">
          <div className="input-group mb-3">
            <input
              required={true}
              type="text"
              className="form-control"
              id="recipeNum"
              placeholder="Number of Recipes"
              aria-label="Recipient's username"
              aria-describedby="button-addon2"
            />
            <button
              className="btn btn-outline-secondary"
              type="button"
              onClick={handleClick}
              id="button-addon2"
            >
              Generate
            </button>
          </div>
          <hr />
          <div className="recipe-card-overflow ">
              {clicked && (
                <div className="recipe-card-container d-grid grid-cols-2 gap-1">
                  {recipes.map((m) => (
                    <RecipeCard key={m.title} recipe={m} />
                  ))}
                </div>
              )}
            </div>
        </div>
      </div>
    );
}

export default RecipeGenerator;
