import { useGetRecipeDetailsQuery } from "../../app/apiSlice";
// import { useEffect } from "react";


const RecipeModal = ({recipe_id, onClose, show}) => {
    const {data, isLoading} = useGetRecipeDetailsQuery(recipe_id)

    // const closeOnEscapePush = (e) => {
    //     if ((e.charCode || e.keyCode) === 27) {
    //         onClose()
    //     }
    // }

    // useEffect(() => {
    //   document.body.addEventListener("keydown", closeOnEscapePush);
    //   return function cleanup() {
    //     document.body.removeEventListener("keydown", closeOnEscapePush);
    //   };
    // }, [closeOnEscapePush]);
    //added above
    if (!show) return null
    if (isLoading) return <div>Loading...</div>

    return (
                    <div className='clown-modal' onClick={onClose}>
                        <div className="clown-modal-content" onClick={e => e.stopPropagation()}>
                            <div className="clown-modal-header">
                                <h3 className="clown-modal-title">{data.title}</h3>
                            </div>
                            <div className="clown-modal-body">
                                    <ul>
                                        {data.servings !== null ? <li>Servings: {data.servings}</li> : <li>Servings: MIssing data </li>}
                                        {data.readyInMinutes !== null ? <li>Prep Time: {data.readyInMinutes} minutes </li> : <li>Prep Time: Missing data</li>}
                                        {data.analyzedInstructions.length > 0 ? <li>Steps: {data.analyzedInstructions[0].steps.length}</li> : <li>Steps: Missing data</li>}
                                    </ul>
                                    <>
                                        <div dangerouslySetInnerHTML={{'__html': data.summary}}/>
                                    </>
                            </div>
                            <div className="clown-modal-footer" >
                                <button className="btn btn-primary" onClick={onClose}>Close</button>
                            </div>
                        </div>
                    </div>
            );
        }

export default RecipeModal;
