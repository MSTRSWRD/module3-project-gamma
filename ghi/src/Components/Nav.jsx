import { NavLink, Link, useNavigate } from "react-router-dom";
import { useGetAccountQuery, useLogoutMutation } from "../app/apiSlice";

const Nav = () => {
    const { data: account } = useGetAccountQuery();
    const [logout] = useLogoutMutation();
    const navigate = useNavigate();

    const handleLogout = () => {
        logout();
        navigate('/PantryPal');
    }

    return (

        <nav className="navbar navbar-expand-lg navbar-light bg-body-secondary">

            <div className="container-fluid">

                <Link to={'/PantryPal'} className="navbar-brand">PantryPal</Link>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="collapseExample">
                    <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                        {account && <li className="nav-item">
                            <NavLink to={'/pantry'} className={'nav-link'}>Pantry</NavLink>
                        </li>}
                        {account && <li className="nav-item">
                            <NavLink to={'/mealplan'} className={'nav-link'}>Meal Plan </NavLink>
                        </li>}
                        {!account && <li className="nav-item">
                            <NavLink to={'/signup'} className={'nav-link'}>Sign Up</NavLink>
                        </li>}
                        {!account && <li className="nav-item">
                            <NavLink to={'/login'} className={'nav-link'}>Login</NavLink>
                        </li>}

                    </ul>
                    {account &&(
                        <button className="btn btn-outline-danger" onClick={handleLogout}>
                            Logout
                        </button>
                    )}
                </div>
            </div>
        </nav>
    )
}

export default Nav;
