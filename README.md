Antonio Cervantes
Walker
Austin
Justin

Design

API design
Account GET - ![Alt text](image.png)
Account POST - ![Alt text](image-1.png)

Authentication POST - ![Alt text](image-2.png)
Authentication DELETE - ![Alt text](image-3.png)

Ingredients GET -  ![Alt text](image-4.png)
Ingredients POST - ![Alt text](image-5.png)
Ingredients PUT - ![Alt text](image-6.png)
Ingredients DELETE - ![Alt text](image-7.png)

Favorites GET - ![Alt text](image-8.png)
Favorites POST - ![Alt text](image-9.png)
Favorites DELETE - ![Alt text](image-10.png)

Meals GET(list) - ![Alt text](image-11.png)
Meals GET(details) - ![Alt text](image-12.png)
Meals PUT - ![Alt text](image-13.png)
Meals DELETE - ![Alt text](image-14.png)
Meals POST - ![Alt text](image-15.png)

Recipes GET(list) - ![Alt text](image-16.png)
Recipes GET(details) - ![Alt text](image-17.png)

Data model
![Alt text](image-18.png)
![Alt text](image-19.png)
![Alt text](image-20.png)

GHI
Excalidraw - https://excalidraw.com/#room=36ca63ec1a820c4150a5,eEk_jitEoyzDwv-n2VjNRA

Integrations

https://spoonacular.com/
SpoonacularAPI

Intended market

This application is for anyone who likes to try new things in the ktichen! Let the computer do all the thinking instead of cooking the same recipes over and over again.

Functionality

Visitors to the site can:

- Create their own personal PantryPal account
- Keep an inventory of their pantry
- Generate recipes based on what they have on hand
- Plan out their week by slotting in their favorite recipes or try something new
