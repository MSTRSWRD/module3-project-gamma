from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from authenticator import authenticator
from routers import accounts, ingredients, favorites, meals, recipes
import os


app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["http://localhost:3000", os.environ.get("CORS_HOST", None)],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/PantryPal")
def home():
    return True


app.include_router(authenticator.router, tags=["Authentication"])
app.include_router(accounts.router, tags=["Accounts"])
app.include_router(ingredients.router, tags=["Ingredients"])
app.include_router(favorites.router, tags=["Favorites"])
app.include_router(meals.router, tags=["Meals"])
app.include_router(recipes.router, tags=["Recipes"])
