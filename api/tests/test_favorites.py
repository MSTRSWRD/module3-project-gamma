from fastapi.testclient import TestClient
from main import app
from queries.favorites import FavoriteQueries
from models import FavoriteIn
from authenticator import authenticator


client = TestClient(app)


def fake_get_current_account_data():
    return {"id": "9999", "full_name": "test user", "username": "fakeusername"}


class FakeFavoriteQueries:
    def create(self, favorite_in: FavoriteIn, account_id: str):
        favorite = favorite_in.dict()
        favorite["id"] = "fake-id-from-db"
        favorite["account_id"] = account_id
        return favorite

    def list_all_for_account(self, account_id: str):
        return [
            {
                "title": "best dang pizza",
                "meal_id": "12345",
                "id": "6789",
                "account_id": account_id,
            }
        ]

    def delete(self, favorite_id: str, account_id: str):
        return True


def test_create_favorite():
    app.dependency_overrides[FavoriteQueries] = FakeFavoriteQueries
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_current_account_data
    favorite_in = {
        "title": "best tacos",
        "meal_id": "123",
    }

    res = client.post("/api/favorites", json=favorite_in)
    data = res.json()

    assert data == {
        "title": "best tacos",
        "meal_id": "123",
        "id": "fake-id-from-db",
        "account_id": "9999",
    }
    assert res.status_code == 200


def test_list_favorites_for_current_account():
    app.dependency_overrides[FavoriteQueries] = FakeFavoriteQueries
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_current_account_data

    res = client.get("/api/favorites")
    data = res.json()

    assert res.status_code == 200
    assert data == {
        "favorites": [
            {
                "title": "best dang pizza",
                "meal_id": "12345",
                "id": "6789",
                "account_id": "9999",
            }
        ]
    }


def test_delete_favorite():
    app.dependency_overrides[FavoriteQueries] = FakeFavoriteQueries
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_current_account_data

    res = client.delete("/api/favorites/123456")
    data = res.json()

    assert res.status_code == 200
    assert {"success": True} == data
