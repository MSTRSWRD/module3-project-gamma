from fastapi.testclient import TestClient
from main import app
from queries.ingredients import IngredientsQueries
from models import IngredientIn
from authenticator import authenticator

client = TestClient(app)


def fake_get_current_account_data():
    return {"id": "1337", "username": "fake-user"}


class FakeIngredientsQueries:
    def get_all(self, account_id: str):
        return [
            {
                "id": "64b089617ce491846d12335c",
                "account_id": account_id,
                "name": "cheese",
            }
        ]

    def create(self, ingredient_in: IngredientIn, account_id: str):
        ingredient = ingredient_in.dict()
        ingredient["id"] = "fake-id-from-db"
        ingredient["account_id"] = account_id
        return ingredient

    def delete(self, ingredient_id: str, account_id: str):
        return True

    def update(
        self, ingredient_id: str, ingredient_in: IngredientIn, account_id: str
    ):
        ingredient = ingredient_in.dict()
        ingredient["account_id"] = account_id
        ingredient["id"] = ingredient_id
        return ingredient


def test_create_ingredient():
    app.dependency_overrides[IngredientsQueries] = FakeIngredientsQueries
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_current_account_data
    ingredient_in = {"name": "flour"}

    res = client.post("/api/ingredients", json=ingredient_in)
    data = res.json()
    assert data == {
        "id": "fake-id-from-db",
        "account_id": "1337",
        "name": "flour",
    }
    assert res.status_code == 200


def test_delete_ingredient():
    app.dependency_overrides[IngredientsQueries] = FakeIngredientsQueries
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_current_account_data

    res = client.delete("/api/ingredients/122345")
    data = res.json()

    assert res.status_code == 200
    assert {"deleted": True} == data


def test_update_ingredient():
    app.dependency_overrides[IngredientsQueries] = FakeIngredientsQueries
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_current_account_data
    ingredient_in = {"name": "milk"}
    res = client.put("/api/ingredients/122345", json=ingredient_in)
    data = res.json()

    assert res.status_code == 200
    assert data == {"name": "milk", "account_id": "1337", "id": "122345"}


def test_get_ingredients():
    app.dependency_overrides[IngredientsQueries] = FakeIngredientsQueries
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_current_account_data

    res = client.get("/api/ingredients")
    data = res.json()

    assert res.status_code == 200
    assert data == {
        "ingredients": [
            {
                "id": "64b089617ce491846d12335c",
                "account_id": "1337",
                "name": "cheese",
            }
        ]
    }
