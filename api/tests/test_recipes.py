from fastapi.testclient import TestClient
from main import app
from queries.recipes import RecipeQueries

client = TestClient(app)


class FakeRecipeQueries:
    def get_recipes(self, ingredients: str, number: int):
        return [{"id": 1, "title": "TACOS", "image": "TACO_PIC"}]

    def get_recipe_detail(self, id: int):
        return {
            "title": "CHOCOLATE PEANUT BUTTER, FROZEN BANANA DIPS",
            "readyInMinutes": 45,
            "servings": 6,
            "image": "https://spoonacular.com/recipeImages/639152-556x370.jpg",
            "summary": 'you can never have...',
            "analyzedInstructions": [],
        }


def test_get_recipes():
    app.dependency_overrides[RecipeQueries] = FakeRecipeQueries
    ingredient = "meat, cheese, tomato"
    number = 1
    res = client.get(
        "/api/recipes", params={"ingredient": ingredient, "number": number}
    )
    data = res.json()
    assert res.status_code == 200
    assert data == {
        "recipes": [
            {"id": 1, "title": "TACOS", "image": "TACO_PIC"},
        ]
    }


def test_get_recipe_detail():
    app.dependency_overrides[RecipeQueries] = FakeRecipeQueries
    id = "639152"
    res = client.get("/api/recipes/details", params={"id": id})
    data = res.json()

    assert res.status_code == 200
    assert data == {
        "title": "CHOCOLATE PEANUT BUTTER, FROZEN BANANA DIPS",
        "readyInMinutes": 45,
        "servings": 6,
        "image": "https://spoonacular.com/recipeImages/639152-556x370.jpg",
        "summary": 'you can never have...',
        "analyzedInstructions": [],
    }
