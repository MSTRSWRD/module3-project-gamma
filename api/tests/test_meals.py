from fastapi.testclient import TestClient
from main import app
from queries.meals import MealQueries
from models import MealsIn
from authenticator import authenticator


client = TestClient(app)


def fake_get_current_account_data():
    return {
        "id": "1",
        "full_name": "testing name",
        "username": "testing@mail.com",
    }


class FakeMealsQueries:
    def create(self, meal_in: MealsIn, account_id: str):
        meal = meal_in.dict()
        meal["id"] = "fake-id-from-db"
        meal["account_id"] = account_id
        return meal

    def get_meals(self, account_id: str):
        return [
            {
                "id": "64b03a1c6665426952ac0425",
                "account_id": account_id,
                "title": "cheese",
                "image": "pic",
                "summary": "text",
                "day_meal_time": [],
            }
        ]

    def delete_meal(self, meal_id: str, account_id: str):
        return True

    def get_meal_by_id(self, meal_id: str):
        return {
            "id": meal_id,
            "account_id": "1",
            "title": "cheese",
            "readyInMinutes": 45,
            "servings": 10,
            "image": "pic",
            "summary": "text",
            "analyzedInstructions": [],
        }


def test_create_meal():
    app.dependency_overrides[MealQueries] = FakeMealsQueries
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_current_account_data
    meal_in = {
        "title": "applesauce",
        "readyInMinutes": 0,
        "servings": 0,
        "image": "string",
        "summary": "string",
        "analyzedInstructions": [{"steps": ["string"]}],
        "day_meal_time": ["string"],
    }
    res = client.post("/api/meals/", json=meal_in)
    data = res.json()

    assert res.status_code == 200
    assert data == {
        "id": "fake-id-from-db",
        "account_id": "1",
        "title": "applesauce",
        "image": "string",
        "summary": "string",
        "day_meal_time": ["string"],
    }


def test_list_meals():
    app.dependency_overrides[MealQueries] = FakeMealsQueries
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_current_account_data

    res = client.get("/api/meals")
    data = res.json()

    assert res.status_code == 200
    assert data == {
        "meals": [
            {
                "id": "64b03a1c6665426952ac0425",
                "account_id": "1",
                "title": "cheese",
                "image": "pic",
                "summary": "text",
                "day_meal_time": [],
            }
        ]
    }


def test_delete_meal():
    app.dependency_overrides[MealQueries] = FakeMealsQueries
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_current_account_data

    res = client.delete("/api/meals/122345")
    data = res.json()

    assert res.status_code == 200
    assert {"success": True} == data
