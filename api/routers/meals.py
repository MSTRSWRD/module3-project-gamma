from fastapi import APIRouter, Depends, HTTPException
from models import MealsIn, MealList, MealsOut
from authenticator import authenticator
from queries.meals import MealQueries

router = APIRouter()


@router.get("/api/meals", response_model=MealList)
def get_meals(
    account_data: dict = Depends(authenticator.get_current_account_data),
    queries: MealQueries = Depends(),
):
    return {"meals": queries.get_meals(account_id=account_data["id"])}


@router.get("/api/meals/{meal_id}", response_model=MealsIn)
def meal_details(
    meal_id: str,
    account_data: dict = Depends(authenticator.get_current_account_data),
    queries: MealQueries = Depends(),
):
    meal = queries.get_meal_by_id(meal_id)
    if meal is None:
        raise HTTPException(status_code=404, detail="meal not found")
    return meal


@router.post("/api/meals/", response_model=MealsOut)
def create_meal(
    meal_in: MealsIn,
    account_data: dict = Depends(authenticator.get_current_account_data),
    queries: MealQueries = Depends(),
):
    return queries.create(meal_in=meal_in, account_id=account_data["id"])


@router.delete("/api/meals/{meal_id}")
def delete(
    meal_id: str,
    account_data: dict = Depends(authenticator.get_current_account_data),
    queries: MealQueries = Depends(),
):
    return {
        "success": queries.delete_meal(
            meal_id=meal_id, account_id=account_data["id"]
        )
    }
