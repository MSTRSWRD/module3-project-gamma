from fastapi import APIRouter, Depends
from models import Favorites, FavoriteOut, FavoriteIn
from authenticator import authenticator
from queries.favorites import FavoriteQueries

router = APIRouter()


@router.post("/api/favorites", response_model=FavoriteOut)
def create_favorite(
    favorite_in: FavoriteIn,
    account_data: dict = Depends(authenticator.get_current_account_data),
    queries: FavoriteQueries = Depends(),
):
    return queries.create(
        favorite_in=favorite_in, account_id=account_data["id"]
    )


@router.get("/api/favorites", response_model=Favorites)
def list_favorites_for_current_account(
    account_data: dict = Depends(authenticator.get_current_account_data),
    queries: FavoriteQueries = Depends(),
):
    return {"favorites": queries.list_all_for_account(account_data["id"])}


@router.delete("/api/favorites/{favorite_id}")
def delete_favorite(
    favorite_id: str,
    account_data: dict = Depends(authenticator.get_current_account_data),
    queries: FavoriteQueries = Depends(),
):
    return {
        "success": queries.delete(
            favorite_id=favorite_id, account_id=account_data["id"]
        )
    }
