from fastapi import APIRouter, Depends, HTTPException
from authenticator import authenticator
from models import DeleteStatus, IngredientOut, IngredientIn, IngredientList
from queries.ingredients import IngredientsQueries

router = APIRouter()


@router.get("/api/ingredients", response_model=IngredientList)
def list_ingredients(
    account_data: dict = Depends(authenticator.get_current_account_data),
    queries: IngredientsQueries = Depends(),
):
    return {"ingredients": queries.get_all(account_id=account_data["id"])}


@router.post("/api/ingredients", response_model=IngredientOut)
def create_ingredient(
    ingredient_in: IngredientIn,
    account_data: dict = Depends(authenticator.get_current_account_data),
    queries: IngredientsQueries = Depends(),
):
    return queries.create(
        ingredient_in=ingredient_in, account_id=account_data["id"]
    )


@router.put("/api/ingredients/{ingredient_id}")
def update_ingredient(
    ingredient_id: str,
    ingredient_in: IngredientIn,
    account_data: dict = Depends(authenticator.get_current_account_data),
    queries: IngredientsQueries = Depends(),
):
    ingredient = queries.update(
        ingredient_id=ingredient_id,
        ingredient_in=ingredient_in,
        account_id=account_data["id"],
    )
    if ingredient is None:
        raise HTTPException(status_code=404, detail="Ingredient not found")
    return ingredient


@router.delete("/api/ingredients/{ingredient_id}", response_model=DeleteStatus)
def delete_ingredient(
    ingredient_id: str,
    account_data: dict = Depends(authenticator.get_current_account_data),
    queries: IngredientsQueries = Depends(),
):
    return {
        "deleted": queries.delete(
            ingredient_id=ingredient_id, account_id=account_data["id"]
        )
    }
