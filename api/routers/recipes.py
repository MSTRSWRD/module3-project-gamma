from fastapi import APIRouter, Depends
from models import RecipeOut, RecipeList
from queries.recipes import RecipeQueries

router = APIRouter()


@router.get("/api/recipes", response_model=RecipeList)
def get_recipes(
    ingredient: str,
    number: int = 3,
    queries: RecipeQueries = Depends(),
):
    return {"recipes": queries.get_recipes(ingredient, number)}


@router.get("/api/recipes/details", response_model=RecipeOut)
def get_recipe_detail(id: int, queries: RecipeQueries = Depends()):
    return queries.get_recipe_detail(id)
