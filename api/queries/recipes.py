import requests
import os

API_KEY = os.environ["API_KEY"]


class RecipeQueries:
    def get_recipes(self, ingredients: str, number: int):
        url = "https://api.spoonacular.com/recipes/findByIngredients"
        params = {
            "apiKey": {API_KEY},
            "ingredients": ingredients,
            "number": number,
        }
        headers = {"Content-Type": "application/json"}
        res = requests.get(url, params=params, headers=headers)
        data = res.json()
        return data

    def get_recipe_detail(self, id: int):
        url = f"https://api.spoonacular.com/recipes/{id}/information?apiKey={API_KEY}" # noqa
        res = requests.get(url)
        data = res.json()
        return data
