from queries.client import MongoQueries
from bson.objectid import ObjectId
from models import IngredientIn


class DuplicateAccountError(ValueError):
    pass


class IngredientsQueries(MongoQueries):
    collection_name = "ingredients"

    def get_all(self, account_id: str):
        results = []
        for ingredient in self.collection.find({"account_id": account_id}):
            ingredient["id"] = str(ingredient["_id"])
            results.append(ingredient)
        return results

    def create(self, ingredient_in: IngredientIn, account_id: str):
        ingredient = ingredient_in.dict()
        ingredient["account_id"] = account_id
        self.collection.insert_one(ingredient)
        ingredient["id"] = str(ingredient["_id"])
        return ingredient

    def update(
        self, ingredient_id: str, ingredient_in: IngredientIn, account_id: str
    ):
        ingredient = ingredient_in.dict()
        ingredient["account_id"] = account_id
        result = self.collection.update_one(
            {"_id": ObjectId(ingredient_id)}, {"$set": ingredient}
        )
        if result.matched_count == 0:
            return None
        ingredient["id"] = ingredient_id
        return ingredient

    def delete(self, ingredient_id: str, account_id: str):
        result = self.collection.delete_one(
            {"_id": ObjectId(ingredient_id), "account_id": account_id}
        )
        return result.deleted_count > 0
