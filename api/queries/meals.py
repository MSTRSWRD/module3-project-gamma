from models import MealsIn
from bson.objectid import ObjectId
from queries.client import MongoQueries


class MealQueries(MongoQueries):
    collection_name = "meals"

    def delete_meal(
        self,
        meal_id: str,
        account_id: str,
    ):
        result = self.collection.delete_one(
            {"_id": ObjectId(meal_id), "account_id": account_id}
        )
        return result.deleted_count > 0

    def get_meals(self, account_id: str):
        results = []
        for meal in self.collection.find({"account_id": account_id}):
            meal["id"] = str(meal["_id"])
            results.append(meal)
        return results

    def create(self, meal_in: MealsIn, account_id: str):
        meal = meal_in.dict()
        meal["account_id"] = account_id
        self.collection.insert_one(meal)
        meal["id"] = str(meal["_id"])
        return meal

    def get_meal_by_id(self, meal_id: str):
        meal = self.collection.find_one({"_id": ObjectId(meal_id)})
        if meal is None:
            return None
        meal["id"] = str(meal["_id"])
        return meal
