from pydantic import BaseModel
from typing import List
from jwtdown_fastapi.authentication import Token


class AccountIn(BaseModel):
    full_name: str
    username: str
    password: str


class AccountOut(BaseModel):
    id: str
    full_name: str
    username: str


class AccountOutWithPassword(AccountOut):
    hashed_password: str


class AccountForm(BaseModel):
    username: str
    password: str


class AccountToken(Token):
    account: AccountOut


class HttpError(BaseModel):
    detail: str


class DeleteStatus(BaseModel):
    deleted: bool


class analyzedInstructions(BaseModel):
    steps: list | None


class RecipeOut(BaseModel):
    title: str | None
    readyInMinutes: int | None
    servings: int | None
    image: str | None
    summary: str | None
    analyzedInstructions: List[analyzedInstructions] | None


class Recipes(BaseModel):
    id: int
    title: str
    image: str


class RecipeList(BaseModel):
    recipes: List[Recipes]


class MealsOut(BaseModel):
    id: str
    account_id: str
    title: str | None
    image: str | None
    summary: str | None
    day_meal_time: list | None


class MealsIn(RecipeOut):
    title: str | None
    day_meal_time: list | None
    readyInMinutes: int | None
    servings: int | None
    image: str | None
    summary: str | None
    analyzedInstructions: List[analyzedInstructions] | None


class MealList(BaseModel):
    meals: List[MealsOut]


class FavoriteIn(BaseModel):
    title: str | None
    meal_id: str | None


class FavoriteOut(FavoriteIn):
    id: str
    account_id: str | None


class Favorites(BaseModel):
    favorites: List[FavoriteOut]


class IngredientIn(BaseModel):
    name: str


class IngredientOut(BaseModel):
    id: str
    account_id: str
    name: str


class IngredientList(BaseModel):
    ingredients: List[IngredientOut]
